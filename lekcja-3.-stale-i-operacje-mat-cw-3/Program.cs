﻿using System;

namespace lekcja_3._stale_i_operacje_mat_cw_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int h;
            int a, b;
            Console.WriteLine("Podaj długość krawędzi: ");

            /* Pobieramy tekst wpisany przez użytkownika na konsoli (użytkownik zatwierdza wprowadzony tekst klawiszem Enter) */
            /* Wszystko co jest wprowadzane na konsoli jest interpretowane jako ciąg znaków */
            String tekstPobranyZKonsoli = Console.ReadLine();
            /* Parsujemy go, czyli dokonujemy konwersji z typu ciągu znaków (String) na tym liczb całkowitych (int) */
            a = int.Parse(tekstPobranyZKonsoli);

            Console.WriteLine("Podaj długość krawędzi b: ");
            /* Krótsza forma operacji parsowania */
            b = int.Parse(Console.ReadLine());

            Console.WriteLine("Podaj długość wysokości h: ");
            h = int.Parse(Console.ReadLine());

            double wynik = 0.5 * h * (a + b);
            Console.WriteLine("Pole trapezu to: {0}", wynik);

            Console.ReadKey();
        }
    }
}
